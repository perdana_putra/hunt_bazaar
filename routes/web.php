<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

// Route::auth();
Route::get('logout/{id}', 'Auth\AuthenticatedSessionController@destroy');
// Auth::routes();
// Route::auth();
Route::group(['middleware' => ['auth']], function() {
    // Route::get('/', function () {
    //     // return view('home.index');
    // });
    Route::redirect('/','/admin/invitee');
    
    Route::GET('admin/invitee/indexdata',
    [
      'as' => 'admininvitee.indexdata',
      'uses' => 'AdminInviteeController@indexData',
    ]);
    Route::GET('admin/invitee',
    [
      'as' => 'admininvitee.index',
      'uses' => 'AdminInviteeController@index',
    ]);
    Route::GET('admin/invitee/create',
    [
      'as' => 'admininvitee.create',
      'uses' => 'AdminInviteeController@create',
    ]);
    Route::POST('admin/invitee',
    [
      'as' => 'admininvitee.store',
      'uses' => 'AdminInviteeController@store',
    ]);
    Route::GET('admin/invitee/{id}',
    [
      'as' => 'admininvitee.show',
      'uses' => 'AdminInviteeController@show',
    ]);
});

Route::GET('invitee/registration/{invite_code}',
[
  'as' => 'invitee.registration',
  'uses' => 'InviteeController@edit',
]);
Route::PUT('invitee/{id}',
[
  'as' => 'invitee.update',
  'uses' => 'InviteeController@update',
]);

Route::POST('designer/selectlist',
[
  'as' => 'designer.selectlist',
  'uses' => 'DesignerController@autoCompleteSelectList',
]);
