<?php

namespace App\Queries\General;

class FieldMap
{
    /**
     * [setFieldMap description]
     * @param [type] $requestdata [description]
     * @param [type] $fieldmap    [description]
     */
    protected function setFieldMap($requestdata, $fieldmap)
    {
        $data = [];
        $userdata = [];
        foreach ($requestdata as $key => $value) {
            $userdata[$key] = $value;
        }
        $userdata = array_intersect_key($userdata,$fieldmap);
        foreach ($userdata as $key => $value) {
                $data[$fieldmap[$key]] = $value;
        }
        return $data;
    }
}