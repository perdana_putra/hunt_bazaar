<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Illuminate\Routing\UrlGenerator;

class InvitationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $invitationCode;
    public $invitatioUrl; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $invitationCode)
    {
        //
        $this->email = $email;
        $this->invitationCode = $invitationCode;
        $this->invitatioUrl = url("invitee/registration/{$this->invitationCode}");
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('invitation@huntbazaar.id')
        ->subject('Hunt Bazaar Invitation')
        ->view('emails.invitation');
    }
}
