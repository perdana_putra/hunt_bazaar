<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Illuminate\Routing\UrlGenerator;

class RegistrationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $invitee;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invitee)
    {
        //
        $this->invitee = $invitee;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('registration@huntbazaar.id')
        ->subject('Hunt Bazaar')
        ->view('emails.registration');
    }
}
