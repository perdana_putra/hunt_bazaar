<?php

namespace App\Modules\InviteeDesigner\Queries;

use App\Modules\InviteeDesigner\Queries\InviteeDesignerEntity;

class InviteeDesignerQuery extends InviteeDesignerEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     */
    public function showInviteeQuery($invitee)
    {
        $query = $this->setEntity()
        ->select('designers.name AS layDesigner')
        ->leftJoin('invitees','invitees.invt_id','=','invitee_designers.invt_id')
        ->leftJoin('designers','designers.desg_id','=','invitee_designers.desg_id')
        ->where('invt_id','=',$invitee)
        ->where('is_active','=',1)
        ->get();
        return $query;
    }
}