<?php

namespace App\Modules\InviteeDesigner\Queries;

use App\Models\InviteeDesigner\InviteeDesigner;

class InviteeDesignerEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new InviteeDesigner());
    }
}