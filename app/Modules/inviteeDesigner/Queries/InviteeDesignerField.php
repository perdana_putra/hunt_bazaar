<?php

namespace App\Modules\InviteeDesigner\Queries;

use Str;
use App\Queries\General\FieldMap;

class InviteeDesignerField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'parInvtId' => 'invt_id',
            'parDesgId' => 'desg_id'
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);

        return $data;
    }
}
