<?php

namespace App\Modules\InviteeDesigner\Logics;

use App\Modules\InviteeDesigner\Logics\InviteeDesignerDataManagerLogic;
use App\Modules\InviteeDesigner\Queries\InviteeDesignerQuery;

class InviteeDesignerLogic
{
	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStoreGuest($request, $datetime)
	{
		return (new InviteeDesignerDataManagerLogic())->storeSaveGuest($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new InviteeDesignerDataManagerLogic())->updateSave($id, $request, $datetime);
	}
	
	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new InviteeDesignerQuery())->showQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new InviteeDesignerQuery())->editQuery($id);
	}
}
