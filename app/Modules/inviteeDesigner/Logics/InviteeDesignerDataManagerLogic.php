<?php

namespace App\Modules\InviteeDesigner\Logics;

use Auth;
use App\Modules\InviteeDesigner\Queries\InviteeDesignerField;
use App\Modules\InviteeDesigner\Queries\InviteeDesignerQuery;
use App\Modules\InviteeDesigner\Queries\InviteeDesignerDataManagerQuery;

class InviteeDesignerDataManagerLogic extends InviteeDesignerDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function storeSave($request, $datetime)
    {
        $request->request->add(['is_active' => 1]);
        $datarequest = (new InviteeDesignerField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new InviteeDesignerQuery())->setEntity());
    }

    /**
     * 
     */
    public function storeSaveGuest($request, $datetime)
    {
        $request->request->add(['is_active' => 1]);
        $datarequest = (new InviteeDesignerField())->setField($request);
        return $this->storeDataSaveGuest($datetime, $datarequest, (new InviteeDesignerQuery())->setEntity());
    }

    /**
     * [updateSave description]
     * @param  [type] $id       [description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function updateSave($id, $request, $datetime)
    {
        $datarequest = (new InviteeDesignerField())->setFieldData($request);
        return $this->updateDataSave($id, $datetime, $datarequest, (new InviteeDesignerQuery())->setEntity());
    }

    /**
     * [delete description]
     * @param  [type] $id       [description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function delete($id, $request, $datetime)
    {
        $request->request->add(['is_active' => 0]);
        $datarequest = (new InviteeDesignerField())->setField($request);
        return $this->deleteDataSave($id, $datetime, $datarequest, (new InviteeDesignerQuery())->setEntity());
    }
}
