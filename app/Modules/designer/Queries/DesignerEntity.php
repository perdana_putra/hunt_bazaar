<?php

namespace App\Modules\Designer\Queries;

use App\Models\Designer\Designer;

class DesignerEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Designer());
    }
}