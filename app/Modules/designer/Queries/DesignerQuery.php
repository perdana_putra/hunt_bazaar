<?php

namespace App\Modules\Designer\Queries;

use App\Modules\Designer\Queries\DesignerEntity;

class DesignerQuery extends DesignerEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [autoCompleteSelectListQuery description]
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function autoCompleteSelectListQuery($query)
    {
        $query = $this->setEntity()
        ->select('name AS text', 'desg_id AS id')
        ->where('is_active','=','1')
        ->where('name','like','%'.$query.'%')
        ->get();
        return $query;
    }
}