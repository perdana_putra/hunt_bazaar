<?php

namespace App\Modules\Designer\Logics;

use App\Modules\Designer\Queries\DesignerQuery;

class DesignerLogic
{
	/**
	 * [doEdit description]
	 */
	public function getAutoCompleteSelectList($id)
	{
		return (new DesignerQuery())->autoCompleteSelectListQuery($id);
	}
}
