<?php

namespace App\Modules\Invitee\Queries;

use Str;
use App\Queries\General\FieldMap;

class InviteeField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'parEmail' => 'email',
            'parName' => 'name',
            'parInvitationCode' => 'invitation_code',
            'parRegistrationCode' => 'registration_code',
            'parDateOfBirth' => 'date_of_birth',
            'parGender' => 'gender',
            'parDesgId' => 'desg_id'
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);

        return $data;
    }
}