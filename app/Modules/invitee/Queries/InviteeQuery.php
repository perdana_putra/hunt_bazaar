<?php

namespace App\Modules\Invitee\Queries;

use App\Modules\Invitee\Queries\InviteeEntity;
use Yajra\Datatables\Datatables;

class InviteeQuery extends InviteeEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    public function indexRecordQuery()
    {
        $query = $this->newEntity();
        $query = $query->select('invt_id AS id','email','name','date_of_birth','gender');
        $query = $query->where('is_active','=',1);
        return $query;
    }

    /**
     * [indexDatatable description]
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function indexDatatable($query)
    {
        $datatable = Datatables::of($query);
        $datatable = $datatable->addColumn('action', function ($query) {
            return view('admin_invitee.option', ['query' => $query]);
        });
        return $datatable->make(true);
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('name AS layName', 'email AS layEmail', 'date_of_birth AS layDateOfBirth','gender AS layGender')
        ->where('invt_id','=',$id)
        ->first();
        return $query;
    }

    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('name AS layName', 'email AS layEmail', 'date_of_birth AS layDateOfBirth','gender AS layGender')
        ->where('invt_id','=',$id)
        ->first();
        return $query;
    }

    public function recordByInvitationCodeQuery($invitationCode, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=',1)
        ->where('invitation_code','=',$invitationCode)
        ->first();
        return $query;
    }

    public function recordByEmailQuery($email, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=',1)
        ->where('email','=',$email)
        ->first();
        return $query;
    }
}