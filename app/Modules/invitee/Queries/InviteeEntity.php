<?php

namespace App\Modules\Invitee\Queries;

use App\Models\Invitee\Invitee;

class InviteeEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Invitee());
    }
}