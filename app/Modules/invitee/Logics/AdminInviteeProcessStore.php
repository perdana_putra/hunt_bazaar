<?php

namespace App\Modules\Invitee\Logics;

use Str;
use App\Modules\Invitee\Logics\InviteeLogic;
use App\Mail\InvitationMail;
use Illuminate\Support\Facades\Mail;

class AdminInviteeProcessStore
{
	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		//cek jika sudah ada maka kirim ulang email dengan invitation code yang sama
		$invitee = (new InviteeLogic())->getRecordByEmail($request->email, ['invitation_code']);
		if((!$invitee) || $invitee->invitation_code == null || $invitee->invitation_code == '')
		{
			//set invitation_code
			$request->request->add(['parInvitationCode' => Str::random(100)]);
			//simpan data
			(new InviteeLogic())->doStore($request, $datetime);
		}
		else
		{
			//set invitation_code
			$request->request->add(['parInvitationCode' => $invitee->invitation_code]);
		}
		//kirim email
		Mail::to($request->parEmail)
		->send(new InvitationMail($request->parEmail, $request->parInvitationCode));

	}
}
