<?php

namespace App\Modules\Invitee\Logics;

use Auth;
use App\Modules\Invitee\Queries\InviteeField;
use App\Modules\Invitee\Queries\InviteeQuery;
use App\Modules\Invitee\Queries\InviteeDataManagerQuery;

class InviteeDataManagerLogic extends InviteeDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function storeSave($request, $datetime)
    {
        $request->request->add(['is_active' => 1]);
        $datarequest = (new InviteeField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new InviteeQuery())->setEntity());
    }

    /**
     * [updateSave description]
     * @param  [type] $id       [description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function updateSaveGuest($id, $request, $datetime)
    {
        $datarequest = (new InviteeField())->setField($request);
        return $this->updateDataSaveGuest($id, $datetime, $datarequest, (new InviteeQuery())->setEntity());
        
    }
}
