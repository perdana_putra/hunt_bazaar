<?php

namespace App\Modules\Invitee\Logics;

use Str;
use App\Mail\RegistrationMail;
use Illuminate\Support\Facades\Mail;
use App\Modules\Invitee\Logics\InviteeLogic;
use App\Modules\InviteeDesigner\Logics\InviteeDesignerLogic;

class InviteeProcessUpdate
{
	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		//set invitation_code
		$request->request->add(['parRegistrationCode' => Str::random(100)]);
		//simpan data
		$invitee = (new InviteeLogic())->doUpdateGuest($id, $request, $datetime);
		//simpan data designer
		foreach ($request->parDesigner as $key => $value) {
			$request->request->add(['parInvtId' => $id]);
			$request->request->add(['parDesgId' => $value]);
			(new InviteeDesignerLogic())->doStoreGuest($request, $datetime);
		}
		// kirim email
		Mail::to($invitee->email)
		->later(now()->addHours(1), new RegistrationMail($invitee));
		return $invitee;
	}
}
