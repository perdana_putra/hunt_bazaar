<?php

namespace App\Modules\Invitee\Logics;

use App\Modules\Invitee\Logics\InviteeDataManagerLogic;
use App\Modules\Invitee\Queries\InviteeQuery;

class InviteeLogic
{
	public function getIndexData()
	{
		$query = (new InviteeQuery())->indexRecordQuery();
		$query = (new InviteeQuery())->indexDatatable($query);
		return $query;
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		return (new InviteeDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdateGuest($id, $request, $datetime)
	{
		return (new InviteeDataManagerLogic())->updateSaveGuest($id, $request, $datetime);
	}
	
	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new InviteeQuery())->editQuery($id);
	}

	public function doShow($id)
	{
		return (new InviteeQuery())->showQuery($id);
	}

	public function getRecordByInvitationCode($invitationCode, $select)
	{
		return (new InviteeQuery())->recordByInvitationCodeQuery($invitationCode, $select);
	}

	public function getRecordByEmail($email, $select)
	{
		return (new InviteeQuery())->recordByEmailQuery($email, $select);
	}
}
