<?php

namespace App\Http\Requests\Invitee;

use Illuminate\Foundation\Http\FormRequest;

class InviteeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parName' => 'required|max:30',
            'parDateOfBirth' => 'required|date',
            'parGender' => 'required|max:1',
            'parDesigner' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'parName' => 'Nama',
            'parDateOfBirth' => 'Tanggal Lahir',
            'parGender' => 'Jenis Kelamin',
            'parDesigner' => 'Desainer Favorit'
        ];
    }

    public function messages()
    {
        return [
            'parName.required' => ':attribute harus diisi',
            'parName.max' => 'Maksimal :max karakter',
            'parDateOfBirth.required' => ':attribute harus diisi',
            'parDateOfBirth.date' => ':attribute diisi dengan format tanggal',
            'parGender.required' => ':attribute harus diisi',
            'parGender.max' => 'Maksimal :max karakter',
            'parDesigner.required' => ':attribute harus diisi',
        ];
    }
}
