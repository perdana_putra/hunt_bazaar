<?php

namespace App\Http\Requests\Invitee;

use Illuminate\Foundation\Http\FormRequest;

class InviteeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parEmail' => 'required|email|unique:App\Models\User,email|max:255',
        ];
    }

    public function attributes()
    {
        return [
            'parEmail' => 'Email',
        ];
    }

    public function messages()
    {
        return [
            'parEmail.required' => ':attribute harus diisi',
            'parEmail.email' => ':attribute harus menggunakan format email yang benar (contoh:email@domain.com)',
            'parEmail.unique' => ':attribute sudah terdaftar',
            'parEmail.max' => 'Maksimal :max karakter',
        ];
    }
}
