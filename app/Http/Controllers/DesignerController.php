<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Designer\Logics\DesignerLogic;

class DesignerController extends Controller
{
    /**
     * [autoCompleteSelectList description]
     * @return [type] [description]
     */
    public function autoCompleteSelectList(Request $request)
    {
        $result = (new DesignerLogic())->getAutoCompleteSelectList($request->q);
        return response()->json(
            [
                "status" => 200,
                "results" => $result
            ],
            200
        );
    }
}
