<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Invitee\Logics\InviteeLogic;
use App\Modules\Invitee\Logics\InviteeProcessUpdate;
use App\Http\Requests\Invitee\InviteeUpdateRequest;

class InviteeController extends Controller
{
    public function edit($invitatioCode)
    {
        //cek kode registrasi sudah generate atau belum
        $invitee = (new InviteeLogic())->getRecordByInvitationCode($invitatioCode,['invt_id AS id','invitation_code','name','registration_code']);
        //jika tidak ada data maka error
        if(!$invitee)
        {
            return view('errors.404');
        }
        //jika belum generate
        if($invitee->registration_code == null || $invitee->registration_code == '')
        {
            $genderList = ['P' => 'Pria', 'W' => 'Wanita'];
            return view('invitee.edit',['invitee' => $invitee, 'genderList' => $genderList]);    
        }
        else //jika sudah generate
        {
            return view('invitee.show',['invitee' => $invitee]);
        }
    }

    /**
     *
     */
    public function update(InviteeUpdateRequest $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new InviteeProcessUpdate())->doUpdate($id, $request, $datetime);
        return view('invitee.show',['invitee' => $result]);
    }
}
