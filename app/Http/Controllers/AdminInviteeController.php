<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Invitee\Logics\InviteeLogic;
use App\Modules\Invitee\Logics\AdminInviteeProcessStore;
use App\Http\Requests\Invitee\InviteeStoreRequest;

class AdminInviteeController extends Controller
{
    public function index()
    {
        return view('admin_invitee.index');
    }

    public function indexData()
    {
        return (new InviteeLogic())->getIndexData();
    }

    public function create()
    {
        return view('admin_invitee.create');
    }

    /**
     * input email undangan
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InviteeStoreRequest $request)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new AdminInviteeProcessStore())->doStore($request, $datetime);
        return redirect('admin/invitee/create')->with('message', config('generalmessages.messages.submitSuccess'));
    }

    public function show($id)
    {
        $result = (new InviteeLogic())->doShow($id);
        return view('admin_invitee.detail',['invitee' => $result]);        
    }
}
