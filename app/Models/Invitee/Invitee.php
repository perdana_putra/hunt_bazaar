<?php

namespace App\Models\Invitee;

use Illuminate\Database\Eloquent\Model;

class Invitee extends Model
{
    protected $primaryKey = 'invt_id';
}
