<?php

namespace App\Models\InviteeDesigner;

use Illuminate\Database\Eloquent\Model;

class InviteeDesigner extends Model
{
    protected $primaryKey = 'invtdesg_id';
}
