<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableInviteeDesigners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitee_designers', function (Blueprint $table) {
            $table->bigIncrements('invtdesg_id');
            $table->bigInteger('createdby')->nullable()->unsigned()->default(null);
            $table->datetime('createdtime')->nullable()->default(null);
            $table->bigInteger('changedby')->nullable()->unsigned()->default(null);
            $table->datetime('changedtime')->nullable()->default(null);
            $table->bigInteger('deletedby')->nullable()->unsigned()->default(null);
            $table->datetime('deletedtime')->nullable()->default(null);
            $table->nullableTimestamps();
            $table->boolean('is_active')->nullable()->default(1);

            $table->bigInteger('invt_id')->nullable()->unsigned()->default(null);
            $table->bigInteger('desg_id')->nullable()->unsigned()->default(null);

            $table->foreign('invt_id')->references('invt_id')->on('invitees')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('desg_id')->references('desg_id')->on('designers')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitee_designers');
    }
}
