<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableInvitee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitees', function (Blueprint $table) {
            $table->bigIncrements('invt_id');
            $table->bigInteger('createdby')->nullable()->unsigned()->default(null);
            $table->datetime('createdtime')->nullable()->default(null);
            $table->bigInteger('changedby')->nullable()->unsigned()->default(null);
            $table->datetime('changedtime')->nullable()->default(null);
            $table->bigInteger('deletedby')->nullable()->unsigned()->default(null);
            $table->datetime('deletedtime')->nullable()->default(null);
            $table->nullableTimestamps();
            $table->boolean('is_active')->nullable()->default(1);

            $table->string('email',30)->nullable()->default(null);
            $table->string('invitation_code',100)->nullable()->default(null);
            $table->string('registration_code',100)->nullable()->default(null);
            $table->string('name',50)->nullable()->default(null);
            $table->date('date_of_birth')->nullable()->default(null);
            $table->string('gender',1)->nullable()->default(null);
            $table->bigInteger('desg_id')->nullable()->unsigned()->default(null);

            $table->foreign('desg_id')->references('desg_id')->on('designers')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitees');
    }
}
