<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDesigner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designers', function (Blueprint $table) {
            $table->bigIncrements('desg_id');
            $table->bigInteger('createdby')->nullable()->unsigned()->default(null);
            $table->datetime('createdtime')->nullable()->default(null);
            $table->bigInteger('changedby')->nullable()->unsigned()->default(null);
            $table->datetime('changedtime')->nullable()->default(null);
            $table->bigInteger('deletedby')->nullable()->unsigned()->default(null);
            $table->datetime('deletedtime')->nullable()->default(null);
            $table->nullableTimestamps();
            $table->boolean('is_active')->nullable()->default(1);
            $table->string('code',3)->nullable()->default(null);
            $table->string('name',30)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designers');
    }
}

