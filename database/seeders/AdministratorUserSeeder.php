<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class AdministratorUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datetime=date('Y-m-d H:i:s');

        $data = ['name' => 'admin', 'email' => 'god@huntbazaar.com', 'password' => 'password','created_at' => $datetime];
        $this->insertUser($data);
    }

    public function insertUser($data)
    {
        $user = (new User())->where('email','=',$data['email'])->first();
        if(!isset($user))
        {
            $user = (new User());
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = bcrypt($data['password']);
            $user->created_at = $data['created_at'];
            $user->save();
            print_r($data['name'].' : user added'.PHP_EOL);
        }
        else
        {
            print_r($data['name'].' : user already exist'.PHP_EOL);
        }
    }
}

