<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\designer\designer;

class DesignerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and designers
        $datas = [
            ['name' => 'Other Stories', 'code' => 'AOS'],
            ['name' => '#FR2', 'code' => 'FR2'],              
            ['name' => '10 Deep', 'code' => '1DE'],               
            ['name' => '3.1 Phillip Lim', 'code' => 'PHI'],               
            ['name' => '32 Paradis', 'code' => '32P'],               
            ['name' => '3X1', 'code' => '3X1'],               
            ['name' => '7 For All Mankind', 'code' => '7FA'],               
            ['name' => 'a.testoni', 'code' => 'ATE'],               
            ['name' => 'Être Cécile', 'code' => 'ETC'],               
            ['name' => 'Études Studio', 'code' => 'ETS'],               
            ['name' => 'fbudi', 'code' => 'FBU'],               
            ['name' => 'lot78', 'code' => 'LOT'],               
            ['name' => 'ostwald helgason', 'code' => 'ost'],

            ['code' => 'ABA', 'name' => 'A Bathing Ape'],
            ['code' => 'ALL', 'name' => 'A.L.C'],
            ['code' => 'APC', 'name' => 'A.P.C'],
            ['code' => 'AWA', 'name' => 'A.W.A.K.E'],
            ['code' => 'APB', 'name' => 'AAPE by A Bathing Ape'],
            ['code' => 'ACG', 'name' => 'ACG'],
            ['code' => 'ACN', 'name' => 'Acne Studios'],
            ['code' => 'ADM', 'name' => 'Adam by Adam Lippes'],
            ['code' => 'ALI', 'name' => 'Adam Lippes'],
            ['code' => 'ADI', 'name' => 'Adidas'],
            ['code' => 'ARO', 'name' => 'Adidas by Rick Owens'],
            ['code' => 'AYY', 'name' => 'Adidas by Y-3 Yohji Yamamoto'],
            ['code' => 'AGA', 'name' => 'Adrian Gan'],
            ['code' => 'APL', 'name' => 'Adrianna Papell'],
            ['code' => 'AGO', 'name' => 'Adriano Goldschmied'],
            ['code' => 'ADR', 'name' => 'Adrienne Landau'],
            ['code' => 'AGB', 'name' => 'Agnès B.'],
            ['code' => 'AAI', 'name' => 'Aidan And Ice'],
            ['code' => 'AID', 'name' => 'Aidan Mattox'],
            ['code' => 'AIG', 'name' => 'Aigner'],
            ['code' => 'AJO', 'name' => 'Air Jordan'],
            ['code' => 'ALA', 'name' => 'Alaïa'],
            ['code' => 'AMI', 'name' => 'Alain Mikli'],
            ['code' => 'AAA', 'name' => 'ALALA'],
            ['code' => 'AFE', 'name' => 'Alberta Ferretti'],
            ['code' => 'ALD', 'name' => 'Aldies'],
            ['code' => 'AEN', 'name' => 'Alejandro Ingelmo'],
            ['code' => 'ALR', 'name' => 'Alessandra Rich'],
            ['code' => 'ADA', 'name' => 'Alessandro Dell\'Acqua'],
            ['code' => 'AWG', 'name' => 'Alexa Wagner'],
            ['code' => 'AMC', 'name' => 'Alexander McQueen'],
            ['code' => 'ALW', 'name' => 'Alexander Wang'],
            ['code' => 'AWH', 'name' => 'Alexander Wang x H&amp;M'],
            ['code' => 'ABI', 'name' => 'Alexandre Birman'],
            ['code' => 'ALE', 'name' => 'Alexis'],
            ['code' => 'ABR', 'name' => 'Alexis Bittar'],
            ['code' => 'ALO', 'name' => 'Alice + Olivia'],
            ['code' => 'ALC', 'name' => 'ALICE by Temperley'],
            ['code' => 'ALM', 'name' => 'Alice McCall'],
            ['code' => 'ADU', 'name' => 'All Dressed Up'],
            ['code' => 'ALS', 'name' => 'AllSaints'],
            ['code' => 'ALY', 'name' => 'Alo Yoga'],
            ['code' => 'ALT', 'name' => 'Altuzarra'],
            ['code' => 'ACA', 'name' => 'Amedeo Canfora'],
            ['code' => 'AMP', 'name' => 'AMI'],
            ['code' => 'AAJ', 'name' => 'Aminah Abdul Jillil'],
            ['code' => 'AMR', 'name' => 'Amiri'],
            ['code' => 'AMJ', 'name' => 'Amo'],
            ['code' => 'AGS', 'name' => 'Ancient Greek Sandals'],
            ['code' => 'AMO', 'name' => 'Andrea Morelli'],
            ['code' => 'ANP', 'name' => 'Andrea Pompilio'],
            ['code' => 'ANI', 'name' => 'Anine Bing'],
            ['code' => 'ADE', 'name' => 'Ann Demeulemeester'],
            ['code' => 'ANN', 'name' => 'Ann Taylor'],
            ['code' => 'ADL', 'name' => 'Anna Dello Russo x H&amp;M'],
            ['code' => 'AHM', 'name' => 'Anna Dello Russo x H&amp;M'],
            ['code' => 'ANM', 'name' => 'Anna Molinari'],
            ['code' => 'ASU', 'name' => 'Anna Sui'],
            ['code' => 'AFO', 'name' => 'Anne Fontaine'],
            ['code' => 'AVH', 'name' => 'Anne Valerie Hash'],
            ['code' => 'ANR', 'name' => 'Anteprima'],
            ['code' => 'ASC', 'name' => 'Anti Social Social Club'],
            ['code' => 'ANT', 'name' => 'Antik Batik'],
            ['code' => 'ATK', 'name' => 'Antik Denim'],
            ['code' => 'AHN', 'name' => 'Anton Heunis'],
            ['code' => 'ANB', 'name' => 'Antonio Berardi'],
            ['code' => 'ANY', 'name' => 'Anya Hindmarch'],
            ['code' => 'APE', 'name' => 'Aperlai'],
            ['code' => 'AAP', 'name' => 'Apiece Apart'],
            ['code' => 'APR', 'name' => 'April 77'],
            ['code' => 'AQC', 'name' => 'Aquascutum'],
            ['code' => 'AQU', 'name' => 'Aquazzura'],
            ['code' => 'AQR', 'name' => 'Aquilano.Rimondi'],
            ['code' => 'AAD', 'name' => 'Arantxa Adi'],
            ['code' => 'ARE', 'name' => 'Area'],
            ['code' => 'ARB', 'name' => 'Armani Baby'],
            ['code' => 'ARC', 'name' => 'Armani Casa'],
            ['code' => 'ARM', 'name' => 'Armani Collezioni'],
            ['code' => 'ARJ', 'name' => 'Armani Jeans'],
            ['code' => 'AJU', 'name' => 'Armani Junior'],
            ['code' => 'ART', 'name' => 'Artel'],
            ['code' => 'ASH', 'name' => 'ASH'],
            ['code' => 'ASP', 'name' => 'Asli Polat'],
            ['code' => 'AMA', 'name' => 'Aston Martin'],
            ['code' => 'ATP', 'name' => 'ATP Atelier'],
            ['code' => 'AUJ', 'name' => 'Au Jour Le Jour'],
            ['code' => 'AXA', 'name' => 'Axel Arigato'],
        ];
        
        foreach ($datas as $key => $data) {
            $designer = (new designer())->where('code','=',$data['code'])->first();
            if($designer)
            {
                print_r($data['name'].' : designer already exist'.PHP_EOL);
            }
            else
            {
                $designer = (new designer());
                $designer->is_active = true;
                $designer->name = $data['name'];
                $designer->code = $data['code'];
                $designer->created_at = date('Y-m-d H:i:s');
                $designer->save();

                print_r($data['name'].' : designer added'.PHP_EOL);
            }
        }

    }
}
