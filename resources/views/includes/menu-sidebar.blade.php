<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>
  @if(!Auth::check())
    <li>
      <a href="{{ URL('login') }}"><i class="fa fa-circle-o"></i><span>Login</span></a>
    </li>
  @endif
  @if(Auth::check())
    @php
      $id = Auth::user()->id;
    @endphp
    <li>
      <a href="{{ URL('logout').'/'.$id }}"><i class="fa fa-circle-o"></i><span>Logout</span></a>
    </li>
  @endif
  @if(Auth::check())
    <li>
      <a href="{{ URL('admin/invitee') }}"><i class="fa fa-circle-o"></i><span>invite</span></a>
    </li>
  @endif
</ul>
