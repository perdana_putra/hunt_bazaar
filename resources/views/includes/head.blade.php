<meta charset="utf-8">
{{ Html::style('css/app.css') }}
@stack('css-top-bootstrap')
@include('includes.bootstrap.bootstrap-css')
@stack('css-bottom-bootstrap')

@stack('css-top-font-awesome')
@include('includes.fontawasome.fontawasome-css')
@stack('css-bottom-font-awesome')

@stack('css-top-ionicons')
@include('includes.ionicon.ionicon-css')
@stack('css-bottom-ionicons')

@stack('css-top-adminLTE')
@include('includes.adminlte.adminlte-css')
@stack('css-bottom-adminLTE')

@stack('css-top-all-skin')
@include('includes.adminlte.adminlte-purpleskinscss')
@stack('css-bottom-all-skin')

@stack('css-top-adminLTE-custom')
{{ Html::style('assets/adminlte/2.3.8/dist/css/AdminLTE-custom.css') }}
{{ Html::style('css/custom.css') }}
@stack('css-bottom-adminLTE-custom')

@stack('css-head-level-1')
@stack('css-head-level-2')
@stack('css-head-level-3')

@stack('js-top-jquery')
@include('includes.jquery.jquery-js')
@stack('js-bottom-jquery')

@stack('js-head-level-1')
@stack('js-head-level-2')
@stack('js-head-level-3')
