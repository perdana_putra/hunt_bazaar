@if(App::environment('test','local'))
{{ Html::style('assets/adminlte/2.3.8/plugins/select2/select2.min.css') }}
@endif
@if(App::environment('live','staging','production'))
{{ Html::style('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css') }}
@endif
