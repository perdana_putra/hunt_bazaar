@if(App::environment('test','local'))
{{ Html::script('assets/adminlte/2.3.8/plugins/select2/select2.full.min.js') }}
@endif
@if(App::environment('live','staging','production'))
{{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js') }}
@endif