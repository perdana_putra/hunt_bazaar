<header class="main-header">
<!-- Logo -->
<a href="{{ URL('/') }}" class="logo">
  <span class="logo-mini">HB</span>
  <span class="logo-lg"><b>HuntBazaar</b></span>
</a>
<nav class="navbar navbar-static-top" role="navigation">
  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>
  <div class="navbar-custom-menu">
  @include('includes.nav-bar')
  </div>
</nav>
</header>
