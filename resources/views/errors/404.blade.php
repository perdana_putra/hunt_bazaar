@extends('guest')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-danger" role="alert">
        Page not found!
        </div>
    </div>
</div>
@stop