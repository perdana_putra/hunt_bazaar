<!DOCTYPE html>
<meta charset="utf-8">
<html>
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Hunt Bazaar</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
      window.Laravel = <?php echo json_encode([
          'csrfToken' => csrf_token(),
      ]); ?>
    </script>
    @include('includes.head')
  </head>
  <body class="hold-transition sidebar-collapse sidebar-mini skin-purple">
    <div class="wrapper">
      @include('includes.header')
      @include('includes.leftsidebar')
      <div class="content-wrapper">
        <section class="content-header" style="height: 35px;">
        </section>
        <section class="content">
        @yield('content')
        </section>
      </div>
    </div>
    @include('includes.foot')
  </body>
</html>
