@extends('guest')
@section('content')
<div class="box box-primary">
  <div class="row">
    <div class="col-lg-12">
      <div class="box-header with-border box-title">
        <h3>Hi, {{ $invitee->name }}</h3>
      </div>
      <div class="box-body">
        <p>
        Terima kasih sudah mendaftarkan diri kamu, berikut adalah kode registrasi kamu:
        </p>
        <p>
          {{ $invitee->registration_code }}
        </p>
      </div>
    </div>
  </div>
</div>
@stop