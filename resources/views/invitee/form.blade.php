
<div class="form-group{{ $errors->has('parName') ? ' has-error' : '' }}">
  {!! Form::label('parName', 'Nama', ['class' => 'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('parName', null, ['class' => 'form-control', 'placeholder' => '']) !!}
    @if ($errors->has('parName'))
      <span class="help-block">
        <strong>{{ $errors->first('parName') }}</strong>
      </span>
    @endif
  </div>
</div>

<div class="form-group{{ $errors->has('parDateOfBirth') ? ' has-error' : '' }}">
  {!! Form::label('parDateOfBirth', 'Tanggal Lahir', ['class' => 'col-md-2 control-label']) !!}
  <div class="col-md-4">
    <div class="input-group date col-md-4">
    {!! Form::text('parDateOfBirth', null, ['class' => 'form-control', 'id' => 'dobdate', 'placeholder' => '']) !!}
      <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
      </span>
    </div>
    @if ($errors->has('parDateOfBirth'))
      <span class="help-block">
        <strong>{{ $errors->first('parDateOfBirth') }}</strong>
      </span>
    @endif
  </div>
</div>

<div class="form-group{{ $errors->has('parGender') ? ' has-error' : '' }}">
  {!! Form::label('parGender', 'Jenis Kelamin', ['class' => 'col-md-2 control-label', 'id' => 'gender']) !!}
  <div class="col-md-4">
    {!! Form::select('parGender', $genderList, null, ['class' => 'form-control', 'placeholder' => '--Pilih Jenis Kelamin--']) !!}
    @if ($errors->has('parGender'))
      <span class="help-block">
        <strong>{{ $errors->first('parGender') }}</strong>
      </span>
    @endif
  </div>
</div>

<div class="form-group{{ $errors->has('parDesigner') ? ' has-error' : '' }}">
  {!! Form::label('parDesigner', 'Desainer Favorit', ['class' => 'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::select('parDesigner[]', [], [], ['class' => 'form-control', 'id'=>'designer-select2']) !!}
    @if ($errors->has('parDesigner'))
      <span class="help-block">
        <strong>{{ $errors->first('parDesigner') }}</strong>
      </span>
    @endif
  </div>
</div>

@push('css-head-level-1')
  @include('includes.select2.select2-css')

  @include('includes.bootstrap.bootstrap-datepicker-css')
@endpush

@push('js-foot-level-1')
  @include('includes.bootstrap.bootstrap-datepicker-js')
  <script type="text/javascript">
    $(function () {
        $('#dobdate').datepicker({
          format:"yyyy-m-d"
        });
    });
  </script>

  @include('includes.select2.select2-js')
  <script type="text/javascript">
    $("#designer-select2").select2({
      placeholder: "Pilih Desainer Favorit",
      template: 'dropdown',
      allowClear: true,
      //id: 'selectdropdown',
      multiple: true,
      minimumInputLength: 1,
      //data: ['aasdakan asdanara', 'mileaska niraer', 'loeane mitola', 'harial xerakt', 'iloas dinreo', 'klora hutrinam', 'pidsan  yukroas', 'junora kortanime', 'lesrax munirpsa', 'ersimate ilito', 'joona jiraar']
      ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
          method: 'POST',
          url: '{{ URL("designer/selectlist") }}',
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          dataType: 'json',
          //quietMillis: 250,
            results: function (data) { // parse the results into the format expected by Select2.
              // since we are using custom formatting functions we do not need to alter the remote JSON data
              return {results: data};
              //return { results: data.items };
          },
          cache: true
        },
    });
  </script>
@endpush
