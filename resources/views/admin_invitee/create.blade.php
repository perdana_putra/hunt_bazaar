@extends('app')
@section('content')
<div class="box box-primary">
  <div class="row">
    <div class="col-lg-12">
      <div class="box-header with-border box-title">
        <h3>Invite Person</h3>
      </div>
      <div class="box-body">
        <a class="btn btn-box btn-success"  style="margin-bottom:10px" href="{{ URL('admin/invitee') }}">kembali</a>
      </div>
      <div class="box-body">
        @if ($errors->any())
        <ul class="alert alert-danger">
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
        @endif
        {!! Form::open(['url' => 'admin/invitee', 'class' => 'form-horizontal', 'invitee' => 'form']) !!}
        {{ csrf_field() }}
        @include('admin_invitee.form')
        <div class="form-group">
          <div class="col-md-2 col-md-offset-2">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@stop
