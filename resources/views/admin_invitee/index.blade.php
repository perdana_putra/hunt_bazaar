@extends('app')
@section('content')

@push('css-head-level-1')
  @include('includes.datatable.datatables-css')
  @include('includes.datatable.datatables-responsive-css')
@endpush
<div class="box box-primary">
  <div class="row">
    <div class="col-lg-12">
      <div class="box-header with-border box-title">
        <h3>Undangan</h3>
      </div>
      <div class="box-body">
        <a class="btn btn-box btn-success"  style="margin-bottom:10px" href="{{ URL('admin/invitee/create') }}">Tambah Undangan</a>
      </div>
      <div class="box-body">
        <table id="table1" class="table table-bordered table-striped" width="100%">
          <thead>
            <tr>
              <th>Email</th>
              <th>Nama</th>
              <th>Tgl Lahir</th>
              <th>Jenis Kelamin</th>
              <th width="90px">Option</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot>
            <tr>
              <th>Email</th>
              <th>Nama</th>
              <th>Tgl Lahir</th>
              <th>Jenis Kelamin</th>
              <th width="90px">Option</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
@push('js-foot-level-1')
  @include('includes.datatable.datatables-js')
  @include('includes.datatable.datatables-responsive-js')
  <script>
    $(function () {
      $("#table1").DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        deferRender: true,
        ajax: '{{ URL('admin/invitee/indexdata') }}',
        columns: [
          { data: 'email', name: 'email' },
          { data: 'name', name: 'name' },
          { data: 'date_of_birth', name: 'date_of_birth' },
          { data: 'gender', name: 'gender' },
          { data: 'action', name: 'action', orderable: false, searchable: false},
        ]
      });
    });
  </script>
@endpush
@stop