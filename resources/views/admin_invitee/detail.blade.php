@extends('app')
@section('content')
<div class="box box-primary">
  <div class="row">
    <div class="col-lg-12">
      <div class="box-header with-border box-title">
        <h3>Detail Undangan</h3>
      </div>
      <div class="box-body">
        <a class="btn btn-box btn-success"  style="margin-bottom:10px" href="{{ URL('admin/invitee') }}">kembali</a>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered detail-view">
        	<tbody>
            <tr>
              <th class="col-lg-1">Nama</th>
              <td>{{ $invitee->layName }}</td>
            </tr>
            <tr>
              <th>Email </th>
              <td>{{ $invitee->layEmail }}</td>
            </tr>
            <tr>
              <th>Tgl Lahir</th>
              <td>{{ $invitee->layDateOfBirth }}</td>
            </tr>
            <tr>
              <th>Jenis Kelamin</th>
              <td>{{ $invitee->layGender }}</td>
            </tr>
        	</tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@stop