
<div class="form-group{{ $errors->has('parEmail') ? ' has-error' : '' }}">
  {!! Form::label('parEmail', 'Email Undangan', ['class' => 'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('parEmail', null, ['class' => 'form-control', 'placeholder' => '']) !!}
    @if ($errors->has('parEmail'))
      <span class="help-block">
        <strong>{{ $errors->first('parEmail') }}</strong>
      </span>
    @endif
  </div>
</div>

<script type="text/javascript">
</script>
